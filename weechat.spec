%global _hardened_build 1
%global __provides_exclude_from ^%{_libdir}/weechat/plugins/.*$
%if %{?_pkgdocdir:1}0
%global _doc %{name}
%else
%global _doc %{name}-%{version}
%global _pkgdocdir %{_docdir}/%{_doc}
%endif
Name:      weechat
Version:   2.7
Release:   1%{?dist}
Summary:   Portable, fast, light and extensible IRC client
URL:       http://weechat.org
Group:     Applications/Communications
License:   GPLv3
Source:    https://weechat.org/files/src/%{name}-%{version}.tar.bz2
# /usr/bin/ld: CMakeFiles/charset.dir/charset.o:
# relocation R_X86_64_PC32 against symbol `weechat_charset_plugin'
# can not be used when making a shared object; recompile with -fPIC
Patch0:    https://src.fedoraproject.org/rpms/weechat/raw/master/f/weechat-1.0.1-plugins-fPIC.patch
BuildRequires: gcc
BuildRequires: asciidoctor >= 1.5.4
BuildRequires: ca-certificates
BuildRequires: cmake
BuildRequires: docbook-style-xsl
BuildRequires: enchant-devel
BuildRequires: gettext
BuildRequires: gnutls-devel
BuildRequires: guile-devel
BuildRequires: libcurl-devel
BuildRequires: libgcrypt-devel
BuildRequires: lua-devel
BuildRequires: ncurses-devel
BuildRequires: perl-ExtUtils-Embed
BuildRequires: perl-devel
BuildRequires: pkgconfig
BuildRequires: python3-devel
BuildRequires: ruby
BuildRequires: ruby-devel
BuildRequires: source-highlight
BuildRequires: tcl-devel
%ifarch %{ix86} x86_64 %{arm}
# https://bugzilla.redhat.com/show_bug.cgi?id=1338728
# https://github.com/weechat/weechat/issues/360
%if 0%{?rhel} || 0%{?fedora} < 25
BuildRequires: v8-devel
%endif
%endif
BuildRequires: zlib-devel
Requires:       hicolor-icon-theme
%description
WeeChat (Wee Enhanced Environment for Chat) is a portable, fast, light and
extensible IRC client. Everything can be done with a keyboard.
It is customizable and extensible with scripts.
%package devel
Summary: Development files for weechat
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: pkgconfig
%description devel
WeeChat (Wee Enhanced Environment for Chat) is a portable, fast, light and
extensible IRC client. Everything can be done with a keyboard.
It is customizable and extensible with scripts.
This package contains include files and pc file for weechat.
%prep
%autosetup -p1 -n %{name}-%{version}
find doc/ -type f -name 'CMakeLists.txt' \
    -exec sed -i -e 's#${PROJECT_NAME}#%{_doc}#g' '{}' \;
%build
mkdir build
pushd build
%cmake \
  -DPREFIX=%{_prefix} \
  -DLIBDIR=%{_libdir} \
  -DENABLE_ENCHANT=ON \
  -DENABLE_DOC=ON \
  -DENABLE_MAN=ON \
  -DENABLE_PHP=OFF \
%if 0%{?fedora} >= 25
  -DENABLE_JAVASCRIPT=OFF \
%endif
  -DCA_FILE=/etc/pki/tls/certs/ca-bundle.crt \
  ..
%make_build VERBOSE=1
popd
%install
rm -rf $RPM_BUILD_ROOT
pushd build
%make_install
popd
%find_lang %name
%files -f %{name}.lang
%doc AUTHORS.adoc ChangeLog.adoc Contributing.adoc
%doc README.adoc ReleaseNotes.adoc
%license COPYING
%{_bindir}/%{name}-curses
%{_bindir}/%{name}
%{_bindir}/%{name}-headless
%{_libdir}/%{name}
%{_datadir}/icons/hicolor/32x32/apps/%{name}.png
%{_pkgdocdir}/weechat_*.html
%{_mandir}/man1/weechat.1*
%{_mandir}/cs/man1/weechat.1*
%{_mandir}/de/man1/weechat.1*
%{_mandir}/fr/man1/weechat.1*
%{_mandir}/it/man1/weechat.1*
%{_mandir}/ja/man1/weechat.1*
%{_mandir}/pl/man1/weechat.1*
%{_mandir}/ru/man1/weechat.1*
%{_mandir}/man1/%{name}-headless.1*
%{_mandir}/*/man1/%{name}-headless.1*
%files devel
%dir %{_includedir}/%{name}
%{_includedir}/%{name}/weechat-plugin.h
%{_libdir}/pkgconfig/*.pc

%changelog